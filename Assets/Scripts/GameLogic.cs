﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class GameLogic : MonoBehaviour, IHasChanged {

	// Use this for initialization
	void Start () {
		HasChanged();
	}
	
	public void HasChanged()
	{
		Debug.Log("Stuff changed");
	}

}

namespace UnityEngine.EventSystems
{
	public interface IHasChanged : IEventSystemHandler
	{
		void HasChanged();
	}
}
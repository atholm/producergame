﻿using UnityEngine;
using System.Collections;

public class LevelSpawner : MonoBehaviour 
{
	public GameObject gameBoard;
	public GameObject actorPrefab;

	private int minX, maxX, minY, maxY;


	// Use this for initialization
	void Start () 
	{
		minX = (int)-gameBoard.GetComponent<RectTransform>().rect.width / 2;
		maxX = (int)gameBoard.GetComponent<RectTransform>().rect.width / 2;
		minY = (int)-gameBoard.GetComponent<RectTransform>().rect.height / 2;
		maxY = (int)gameBoard.GetComponent<RectTransform>().rect.height / 2;
	}

	void PlaceObject()
	{
		int posX = (int)Random.Range(minX, maxX);
		int posY = (int)Random.Range(minY, maxY);

		GameObject clone = GameObject.Instantiate(actorPrefab);
		clone.transform.parent = gameBoard.transform;
		RectTransform cloneRect = clone.GetComponent<RectTransform>();
		cloneRect.anchoredPosition = new Vector2(posX, posY);
		cloneRect.localScale = new Vector3(1,1,1);

		Rect rect1 = new Rect(cloneRect.anchoredPosition.x, cloneRect.anchoredPosition.y, cloneRect.rect.width, cloneRect.rect.height);

		foreach (RectTransform child in gameBoard.GetComponentInChildren<RectTransform>()) 
		{
			if(child.tag == "actor")
			{
				Rect rect2 = new Rect(child.anchoredPosition.x, child.anchoredPosition.y, child.rect.width, child.rect.height);
				if(RectsOverlap(rect1, rect2) || !RectInsideCanvas(rect1))
				{
					Destroy(clone);
					PlaceObject();
					return;
				}
			}
		}
		clone.tag = "actor";
	}

	bool RectInsideCanvas(Rect rect1)
	{
		int sizeDelta = (int)rect1.width / 2;
		if( (rect1.x > (minX + sizeDelta)) && 
		   	(rect1.x < (maxX - sizeDelta)) &&
		   	(rect1.y > (minY + sizeDelta)) && 
		   	(rect1.y < (maxY - sizeDelta)) )
			return true;
		else
			return false;
		   
	}

	bool RectsOverlap(Rect rect1, Rect rect2) 
	{
		return  rect1.Contains(new Vector2(rect2.x, rect2.y)) ||
				rect1.Contains(new Vector2(rect2.x + rect2.width, rect2.y)) ||
				rect1.Contains(new Vector2(rect2.x, rect2.y + rect2.height)) ||
				rect1.Contains(new Vector2(rect2.x + rect2.width, rect2.y + rect2.height));
	}

	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyUp(KeyCode.Space))
		{
			PlaceObject();
		}
	}
}

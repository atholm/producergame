﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class InputHandler : MonoBehaviour, IPointerClickHandler {

	public GameObject line;
	public GameObject bar;
	private bool barActive = false;
	public void OnPointerClick(PointerEventData eventData)
	{
		line.GetComponent<LineRenderer>().enabled =true;
		barActive = true;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(barActive)
		{
			bar.GetComponent<EnergyBar>().valueCurrent++;
		}
	}
	
	
}
